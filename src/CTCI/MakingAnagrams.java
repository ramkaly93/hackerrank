package CTCI;

import java.util.Scanner;

public class MakingAnagrams {
	public static int numberNeeded(String first, String second) {

		StringBuilder sb1 = new StringBuilder(first);
		StringBuilder sb2 = new StringBuilder(second);

		for (int i = 0; i < first.length(); i++) {

			char c = first.charAt(i);
			int id1 = sb1.indexOf(c + "");
			int id2 = sb2.indexOf(c + "");

			if (id2 != -1) {

				sb1.deleteCharAt(id1);
				sb2.deleteCharAt(id2);
			}

		}

		return sb1.length() + sb2.length();

	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String first = in.next();
		String second = in.next();
		System.out.println(numberNeeded(first, second));

	}

}
