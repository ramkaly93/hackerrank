package ProjectEuler;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

class Reader {
	static BufferedReader reader;
	static StringTokenizer tokenizer;

	/** call this method to initialize reader for InputStream */
	static void init(InputStream input) {
		reader = new BufferedReader(new InputStreamReader(input));
		tokenizer = new StringTokenizer("");
	}

	/** get next word */
	static String next() throws IOException {
		while (!tokenizer.hasMoreTokens()) {

			tokenizer = new StringTokenizer(reader.readLine());
		}
		return tokenizer.nextToken();
	}

	static int nextInt() throws IOException {
		return Integer.parseInt(next());
	}

	static long nextLong() throws IOException {
		return Long.parseLong(next());
	}

	static double nextDouble() throws IOException {
		return Double.parseDouble(next());
	}
}

public class Multiplesof3and5 {

	public static void main(String[] args) throws IOException {
		Reader.init(System.in);
		PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(System.out)));

		long T = Reader.nextLong();

		for (int t = 0; t < T; t++) {

			long N = Reader.nextLong();
			N = N - 1;
			long no5 = N / 5;
			long no3 = N / 3;
			long no15 = N / 15;

			long sum3 = (no3 * ((2 * 3) + ((no3 - 1) * 3))) / 2;
			long sum5 = (no5 * ((2 * 5) + ((no5 - 1) * 5))) / 2;
			long sum15 = (no15 * ((2 * 15) + ((no15 - 1) * 15))) / 2;

			out.println(sum3 + sum5 - sum15);

		}
		out.flush();

	}

}
