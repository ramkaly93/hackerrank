package practice;

import java.util.Scanner;

public class FairRations {
	public static void display(int[] arr) {

		System.out.println("contents of the array");
		for (int i : arr)
			System.out.print(i + " ");
		System.out.println();

	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int N = in.nextInt();
		String ip = "";
		int count = 0;
		for (int B_i = 0; B_i < N; B_i++) {
			int temp = (in.nextInt()) % 2;
			ip += temp + "";
			if (temp == 1)
				count++;

		}

		if (count % 2 == 0) {
			int res = 0;
			for (int i = 0; i < ip.length(); i++) {

				if (ip.charAt(i) == '1') {

					int right;

					right = ip.indexOf('1', i + 1);
					res += (right - i) * 2;
					i = right;

				}

			}

			System.out.println(res);
		}

		else
			System.out.println("NO");

	}

}
