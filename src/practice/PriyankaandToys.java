package practice;

import java.util.Arrays;
import java.util.Scanner;

public class PriyankaandToys {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		int N = scan.nextInt();
		int[] A = new int[N];
		for (int i = 0; i < N; i++) {
			A[i] = scan.nextInt();

		}

		Arrays.sort(A);
		int count = 1;
		int start_price = A[0];
		int end_price = start_price + 4;

		for (int i = 1; i < N; i++) {

			if (A[i] >= start_price & A[i] <= end_price) {

				continue;

			} else {
				count++;
				start_price = A[i];
				end_price = start_price + 4;

			}
		}

		System.out.println(count);

	}

}
