package practice;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Stack;
import java.util.StringTokenizer;

class Index {

	int i;
	int j;

	public Index(int i, int j) {
		super();
		this.i = i;
		this.j = j;
	}

}

public class ConnectedCells {

	static int[][] mat;

	static void display(int rows, int cols) {
		for (int i = 0; i < rows; i++) {

			for (int j = 0; j < cols; j++) {

				System.out.print(mat[i][j] + "  ");
			}
			System.out.println();
		}

	}

	static int dfs(Index id, int row, int col) {
		int regionLen = 0;
		Stack<Index> st = new Stack<Index>();

		st.push(id);
		while (!st.isEmpty()) {
			Index actualIndex = st.pop();
			int i = actualIndex.i;
			int j = actualIndex.j;
			mat[i][j] = -1;
			regionLen++;

			if (i > 0 && j > 0 && mat[i - 1][j - 1] == 1) {
				st.push(new Index(i - 1, j - 1));
				mat[i - 1][j - 1] = 0;
			}
			if (i > 0 && mat[i - 1][j] == 1) {
				st.push(new Index(i - 1, j));
				mat[i - 1][j] = 0;

			}
			if (i > 0 && j < col - 1 && mat[i - 1][j + 1] == 1) {
				st.push(new Index(i - 1, j + 1));
				mat[i - 1][j + 1] = 0;

			}
			if (j > 0 && mat[i][j - 1] == 1) {
				st.push(new Index(i, j - 1));
				mat[i][j - 1] = 0;
			}
			if (j < col - 1 && mat[i][j + 1] == 1) {
				st.push(new Index(i, j + 1));
				mat[i][j + 1] = 0;
			}
			if (i < row - 1 && j > 0 && mat[i + 1][j - 1] == 1) {
				st.push(new Index(i + 1, j - 1));
				mat[i + 1][j - 1] = 0;
			}
			if (i < row - 1 && mat[i + 1][j] == 1) {
				st.push(new Index(i + 1, j));
				mat[i + 1][j] = 0;
			}
			if (i < row - 1 && j < col - 1 && mat[i + 1][j + 1] == 1) {
				st.push(new Index(i + 1, j + 1));
				mat[i + 1][j + 1] = 0;
			}
		}

		// display(row, col);

		// System.out.println("regionlen" + regionLen + "\n\n");
		return regionLen;

	}

	public static void main(String[] args) throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		StringTokenizer st = new StringTokenizer(br.readLine());

		int rows = Integer.parseInt(st.nextToken());

		st = new StringTokenizer(br.readLine());
		int cols = Integer.parseInt(st.nextToken());

		mat = new int[rows][cols];

		for (int i = 0; i < rows; i++) {
			st = new StringTokenizer(br.readLine());

			for (int j = 0; j < cols; j++) {

				mat[i][j] = Integer.parseInt(st.nextToken());

			}
		}
		int max = Integer.MIN_VALUE;
		for (int i = 0; i < rows; i++) {

			for (int j = 0; j < cols; j++) {

				if (mat[i][j] == 1)
					max = Math.max(max, dfs(new Index(i, j), rows, cols));

			}

		}

		PrintWriter pr = new PrintWriter(new OutputStreamWriter(System.out));
		pr.println(max);
		pr.flush();

	}

}
