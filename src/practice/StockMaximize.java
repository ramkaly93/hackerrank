package practice;

import java.util.Scanner;

public class StockMaximize {
	public static int findMax(int index, long[] arr) {

		int max = index;

		for (int i = index; i < arr.length; i++)
			if (arr[i] > arr[max]) {
				max = i;
			}

		max = (max == index) ? -1 : max;

		// System.out.println("max is : " +max);

		return max;

	}

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);

		int T = scan.nextInt();
		String res = "";
		for (int t = 0; t < T; t++) {
			int N = scan.nextInt();
			long[] arr = new long[N];

			for (int i = 0; i < N; i++)
				arr[i] = scan.nextInt();

			long profit = 0;
			int vIndex = -2;
			for (int i = 0; i < N; i++) {

				if (vIndex != -2 & vIndex != -1 & i < vIndex) {
					profit += arr[vIndex] - arr[i];

				}
				if (vIndex == -2 | i >= vIndex) {
					vIndex = findMax(i, arr);

					if (vIndex == -1)
						vIndex = -2;
					else

						profit += arr[vIndex] - arr[i];

				}

				// System.out.println("profit is : "+ profit);

			}
			res += profit + "\n";

		}
		System.out.println(res);

	}

}
