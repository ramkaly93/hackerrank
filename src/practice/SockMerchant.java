package practice;

import java.util.Hashtable;
import java.util.Scanner;

public class SockMerchant {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n1 = in.nextInt();

		int count = 0;
		Hashtable ht = new Hashtable();
		for (int c_i = 0; c_i < n1; c_i++) {
			int n = in.nextInt();

			if (ht.containsKey(n)) {
				ht.remove(n);
				count++;

			} else
				ht.put(n, 0);

		}
		System.out.println(count);

	}

}
