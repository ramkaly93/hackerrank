package practice;

import java.util.Scanner;

public class Flippingbits {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int T = scanner.nextInt();
		for (int t = 0; t < T; t++) {

			String ip = Long.toBinaryString(scanner.nextLong());

			while (ip.length() != 32)
				ip = "0" + ip;
			String res = "";
			for (int i = 0; i < ip.length(); i++)
				res += (ip.charAt(i) == '0') ? '1' : '0';

			// System.out.println(res);
			System.out.println(Long.parseLong(res, 2));

		}
	}

}
