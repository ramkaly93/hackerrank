package practice;

import java.util.Scanner;

public class NikitaandtheGame {
	public static int partialSumIndex(long[] arr, long sum, int i, int N) {

		long cSum = 0;
		for (; i < N; i++) {
			cSum += arr[i];

			if (cSum == sum)
				return i + 1;

		}

		return -1;

	}

	public static long calculateSum(long[] arr, int start, int N) {
		long sum = 0;

		for (int i = start; i < N; i++)
			sum += arr[i];

		return sum;

	}

	public static long maxScore(long[] arr, int start, int N) {

		// System.out.print("\nstart" + start+" N " +N);
		long sum = calculateSum(arr, start, N);
		// System.out.print("sum " +sum+ " ");

		if (sum % 2 != 0)
			return 0;
		else if (N - start <= 2)
			return N - start - 1;
		else {
			int mid = partialSumIndex(arr, sum / 2, start, N);

			// System.out.println(" mid: "+ mid);

			if (mid != -1) {
				long leftscore = maxScore(arr, start, mid);
				long rightscore = maxScore(arr, mid, N);
				return Math.max(leftscore, rightscore) + 1;
			} else
				return 0;
		}
	}

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int T = scan.nextInt();
		String res = "";
		for (int t = 0; t < T; t++) {
			int N = scan.nextInt();
			long[] arr = new long[N];
			int i = 0;
			for (; i < N; i++)
				arr[i] = scan.nextLong();

			// System.out.println(" starting the processing ");
			res += maxScore(arr, 0, arr.length) + "\n";

		}

		System.out.println(res);
	}

}
