package practice;

import java.util.Scanner;

public class MarsExploration {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String S = in.next();
		int len = S.length();
		int msgs = len / 3;
		String eout = "";
		int cnt = 0;
		for (int i = 0; i < msgs; i++, eout += "SOS")
			;

		for (int i = 0; i < len; i++) {

			if (eout.charAt(i) != S.charAt(i))
				cnt++;

		}

		System.out.println(cnt);
	}

}
