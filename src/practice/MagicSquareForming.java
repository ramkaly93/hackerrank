package practice;

import java.util.Scanner;

public class MagicSquareForming {
	static int returnCost(int[][] arr1, int[][] arr2) {
		int cost = 0;
		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)

				cost += Math.abs(arr1[i][j] - arr2[i][j]);

		return cost;

	}

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);

		int[][] arr1 = { { 8, 1, 6 }, { 3, 5, 7 }, { 4, 9, 2 } };
		int[][] arr2 = { { 4, 3, 8 }, { 9, 5, 1 }, { 2, 7, 6 } };
		int[][] arr3 = { { 2, 9, 4 }, { 7, 5, 3 }, { 6, 1, 8 } };
		int[][] arr4 = { { 6, 7, 2 }, { 1, 5, 9 }, { 8, 3, 4 } };
		int[][] arr5 = { { 6, 1, 8 }, { 7, 5, 3 }, { 2, 9, 4 } };
		int[][] arr6 = { { 8, 3, 4 }, { 1, 5, 9 }, { 6, 7, 2 } };
		int[][] arr7 = { { 4, 9, 2 }, { 3, 5, 7 }, { 8, 1, 6 } };
		int[][] arr8 = { { 2, 7, 6 }, { 9, 5, 1 }, { 4, 3, 8 } };

		int[][] arr = new int[3][3];
		int mincost = 10000, cost = 0;

		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)

				arr[i][j] = scan.nextInt();

		cost = returnCost(arr, arr1);
		if (cost < mincost)
			mincost = cost;

		cost = returnCost(arr, arr2);
		if (cost < mincost)
			mincost = cost;

		cost = returnCost(arr, arr3);
		if (cost < mincost)
			mincost = cost;

		cost = returnCost(arr, arr4);
		if (cost < mincost)
			mincost = cost;

		cost = returnCost(arr, arr5);
		if (cost < mincost)
			mincost = cost;

		cost = returnCost(arr, arr6);
		if (cost < mincost)
			mincost = cost;

		cost = returnCost(arr, arr7);
		if (cost < mincost)
			mincost = cost;

		cost = returnCost(arr, arr8);
		if (cost < mincost)
			mincost = cost;

		System.out.println(mincost);

	}

}
