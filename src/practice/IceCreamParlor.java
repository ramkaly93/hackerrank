package practice;

import java.util.Arrays;
import java.util.Scanner;

public class IceCreamParlor {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);

		int T = scan.nextInt();
		String res = "";
		for (int t = 0; t < T; t++) {

			int M = scan.nextInt();
			int N = scan.nextInt();

			int arr[] = new int[N];
			int uarr[] = new int[N];

			for (int i = 0; i < N; i++) {
				arr[i] = scan.nextInt();
				uarr[i] = arr[i];
			}

			Arrays.sort(arr);

			for (int i = 0; arr[i] < M; i++) {

				int ind1 = i;
				int tmp = Math.abs(M - arr[i]);
				/*
				 * if (tmp == arr[i]) { continue; }
				 */
				int ind2 = Arrays.binarySearch(arr, 0, N, tmp);

				// System.out.println("ind1 " + arr[ind1] + "ind2 " +
				// arr[ind2]);
				if ((ind1 >= 0) && (ind2 >= 0)) {

					int aind1 = 0, aind2 = 0;
					for (int j = 0; j < N; j++) {

						if (arr[ind1] == uarr[j]) {
							aind1 = j + 1;
						}

					}

					for (int j = 0; j < N; j++) {
						if ((arr[ind2] == uarr[j]) & (j != aind1 - 1)) {
							aind2 = j + 1;
						}

					}
					// System.out.println("aind1 " + aind1 + "ind2 " + aind2);
					if (aind1 > aind2) {
						res += aind2 + " " + aind1 + "\n";
						break;

					} else {
						res += aind1 + " " + aind2 + "\n";
						break;

					}
				}

			}

		}

		System.out.println(res);

	}

}
