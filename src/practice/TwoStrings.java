package practice;

import java.util.Hashtable;
import java.util.Scanner;

public class TwoStrings {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		int T = scan.nextInt();
		String fres = "";

		for (int t = 0; t < T; t++) {

			String first = scan.next();
			String second = scan.next();
			Hashtable ht = new Hashtable();
			char[] small = (first.length() > second.length()) ? second.toCharArray() : first.toCharArray();
			char[] large = (first.length() > second.length()) ? first.toCharArray() : second.toCharArray();

			String tres = "NO\n";

			for (int i = 0; i < small.length; i++) {

				ht.put(small[i], 1);
			}

			for (int i = 0; i < large.length; i++) {

				if (ht.containsKey(large[i])) {

					tres = "YES\n";
					break;
				}

			}

			fres += tres;

		}
		System.out.println(fres);

	}

}
