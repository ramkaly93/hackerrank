package practice;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Scanner;

public class GridlandMetro {
	static class Index {

		long startPos;
		long endPos;

		public Index(long startPos, long endPos) {
			super();
			this.startPos = startPos;
			this.endPos = endPos;
		}

	}

	static Hashtable<Long, ArrayList<Index>> ht = new Hashtable<Long, ArrayList<Index>>();

	public static void main(String[] args) throws IOException {

		Scanner scan = new Scanner(System.in);
		long n = scan.nextLong();
		long m = scan.nextLong();
		BigInteger val = new BigInteger(n + "").multiply(new BigInteger(m + ""));
		long k = scan.nextLong();

		for (int i = 0; i < k; i++) {

			long r = scan.nextLong();
			long c1 = scan.nextLong();
			long c2 = scan.nextLong();

			String v = findlen(r, c1, c2) + "";

			val = val.subtract(new BigInteger(v));

			// System.out.println("curr val " +val);

			// System.out.println("\n\n");
			// val = val - ;
			// System.out.println(val);
		}

		System.out.println(val);

	}

	private static long findlen(long r, long c1, long c2) {

		// System.out.println(r + " , " + c1 + " , " + c2);

		Index id = new Index(c1, c2);
		if (ht.containsKey(r)) {

			ArrayList<Index> ar = ht.get(r);

			for (int i = 0; i < ar.size(); i++) {

				Index curr = ar.get(i);

				// System.out.print("curr.startPos: " + curr.startPos +
				// "curr.endPos: " + curr.endPos);
				// System.out.println("c1: " + c1 + "c2: " + c2);

				// completely taken it
				if (curr.startPos <= c1 && curr.endPos >= c2) {
					// System.out.println("c1 & c2 completely inside curr
					// element");

					return 0;
				}

				else if (curr.startPos >= c1 && curr.endPos <= c2) {
					// System.out.println(
					// "curr.start and curr.end completely inside c1 and c2 , c1
					// and c2 covers existing element");

					Index mergedIndex = new Index(c1, c2);
					ar.add(mergedIndex);
					ar.remove(i);
					ht.put(r, ar);
					return ((curr.startPos - c1) + (c2 - curr.endPos));

				}

				// touching it or nearyby bastard;

				else if (curr.endPos == c1) {
					// System.out.println("curr.end is equal to c1");
					Index mergedIndex = new Index(curr.startPos, c2);
					ar.add(mergedIndex);
					ar.remove(i);
					ht.put(r, ar);
					return (c2 - c1);
				}

				else if (curr.endPos + 1 == c1) {
					// System.out.println("curr.end+1 is equal to c1");
					Index mergedIndex = new Index(curr.startPos, c2);
					ar.add(mergedIndex);
					ar.remove(i);
					ht.put(r, ar);
					return (c2 - c1 + 1);
				}

				else if (curr.startPos == c2 + 1) {
					// System.out.println("curr.start is equal to c2");
					Index mergedIndex = new Index(c1, curr.endPos);
					ar.add(mergedIndex);
					ar.remove(i);
					ht.put(r, ar);
					return (c2 - c1 + 1);
				}

				else if (curr.startPos == c2) {
					// System.out.println("curr.start is equal to c2");
					Index mergedIndex = new Index(c1, curr.endPos);
					ar.add(mergedIndex);
					ar.remove(i);
					ht.put(r, ar);
					return (c2 - c1);
				}

				else if (curr.startPos <= c1 & curr.endPos >= c1) {
					// System.out.println("F6");

					Index mergedIndex = new Index(curr.startPos, c2);
					long alreadyCov = curr.endPos - c1;
					ar.add(mergedIndex);
					ar.remove(i);
					ht.put(r, ar);

					return (c2 - c1 - alreadyCov);
				}

				else if (curr.startPos >= c1 & curr.endPos >= c2) {
					// System.out.println("F7");

					Index mergedIndex = new Index(c1, curr.endPos);
					long alreadyCov = c2 - curr.startPos;
					ar.add(mergedIndex);
					ar.remove(i);
					ht.put(r, ar);

					return (c2 - c1 - alreadyCov);
				}

				else {
					/*
					 * System.out.println("might be need to handel me");
					 * 
					 * for(int j =0;j<ar.size();j++)
					 * System.out.print(ar.get(j).startPos+ "  "
					 * +ar.get(j).endPos + "\n");
					 * 
					 * System.out.println("\n\n");
					 */
					ar.add(id);
					ht.put(r, ar);
					return (c2 - c1 + 1);

				}
			}

		} else {
			// System.out.println("alwasy in else");

			ArrayList<Index> ar = new ArrayList<Index>();
			ar.add(id);

			ht.put(r, ar);

			return (c2 - c1 + 1);
		}

		return 0;
	}

}
