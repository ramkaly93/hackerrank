package practice;

import java.util.Scanner;

public class SuperReducedString {
	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		String ip = scan.next();
		StringBuilder res = new StringBuilder(ip.charAt(0) + "");

		for (int i = 1; i < ip.length(); i++) {

			if (res.length() > 0 && (ip.charAt(i) == res.charAt(res.length() - 1)))
				res.deleteCharAt(res.length() - 1);

			else
				res.append(ip.charAt(i));

		}
		if (res.length() == 0)
			System.out.println("Empty String");
		else
			System.out.println(res);

	}

}
