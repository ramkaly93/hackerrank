package practice;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Gemstones {
	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		Set<Character> U = new HashSet<Character>();

		for (Character c = 'a'; c <= 'z'; c++)
			U.add(c);

		int N = scan.nextInt();

		for (int n = 0; n < N; n++) {

			String ip = scan.next();
			Set<Character> set = new HashSet<Character>();
			for (int i = 0; i < ip.length(); i++) {

				set.add(ip.charAt(i));

			}

			U.retainAll(set);
			// System.out.println(U);

		}
		System.out.println(U.size());

	}

}
