package practice;

import java.util.HashMap;
import java.util.Scanner;

public class BirthdayCakeCandles {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		HashMap ht = new HashMap();
		int n = scan.nextInt();
		int max = -1;

		for (int i = 0; i < n; i++) {

			Integer ip = scan.nextInt();

			if (ip > max)
				max = ip;
			if (ht.containsKey(ip)) {

				Integer val = (Integer) ht.get(ip);
				ht.remove(ip);
				val = val + 1;
				ht.put(ip, val);

			} else

				ht.put(ip, new Integer(1));

		}

		System.out.println(ht.get(max));

	}

}
