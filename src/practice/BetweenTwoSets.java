package practice;



import java.util.Arrays;
import java.util.Iterator;
import java.util.Scanner;
import java.util.TreeSet;

public class BetweenTwoSets {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int m = in.nextInt();
		int[] a = new int[n];
		for (int a_i = 0; a_i < n; a_i++) {
			a[a_i] = in.nextInt();
		}
		int[] b = new int[m];
		for (int b_i = 0; b_i < m; b_i++) {
			b[b_i] = in.nextInt();
		}

		TreeSet<Integer> xes = new TreeSet<Integer>();
		Arrays.sort(a);
		Arrays.sort(b);
		int max = a[a.length - 1];

		int i = 1;
		while (true) {

			Integer prod = i * max;
			if (prod <= 100 & prod <= b[0]) {
				xes.add(prod);
				i++;
			} else
				break;

		}

		Iterator iter = xes.iterator();

		while (iter.hasNext()) {
			Integer num = (Integer) iter.next();
			boolean flag=false;
			for (int j = 0; j < a.length; j++) {
				
			if(num%a[j]!=0){
				flag=true;
				break;
			}
			}
			if(flag)
				iter.remove();
			
		}
		// System.out.println("xes" + xes);

		int count = 0;
		iter = xes.iterator();
		while (iter.hasNext()) {

			Integer num = (Integer) iter.next();
			boolean flag = true;

			for (int j = 0; j < b.length; j++) {

				if (b[j] % num != 0) {
					flag = false;
					break;
				}

			}
			if (flag)
				count++;

		}
		System.out.println(count);
	}

}

