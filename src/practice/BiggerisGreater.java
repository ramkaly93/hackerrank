package practice;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Scanner;

public class BiggerisGreater {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int T = scan.nextInt();

		PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(System.out)), true);

		for (int t = 0; t < T; t++) {
			boolean flag = false;
			StringBuilder sb = new StringBuilder(scan.next());
			// System.out.println(sb.length());
			label: for (int i = sb.length() - 1; i >= 1; i--) {

				if (sb.charAt(i) > sb.charAt(i - 1)) {
					flag = true;

					char[] arr = sb.substring(i).toCharArray();

					Arrays.sort(arr);
					char c = sb.charAt(i - 1);
					sb.delete(i, sb.length());

					for (int k = 0; k < arr.length; k++) {

						if (arr[k] > c) {
							sb.setCharAt(i - 1, arr[k]);
							arr[k] = c;
							break;
						}
					}
					sb.insert(i, arr);
					break label;

				}

			}
			if (flag)
				out.println(sb);
			else
				out.println("no answer");

		}
		out.flush();

	}

}
