package practice;

import java.math.BigInteger;
import java.util.Scanner;

public class FibonacciModified {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		BigInteger t0 = new BigInteger("" + scan.nextInt());
		BigInteger t1 = new BigInteger("" + scan.nextInt());
		int n = scan.nextInt();
		BigInteger t2 = new BigInteger("0");

		for (int i = 0; i < n - 2; i++) {

			t2 = t0.add(t1.multiply(t1));
			t0 = t1;
			t1 = t2;

		}
		System.out.println(t2);

	}

}
