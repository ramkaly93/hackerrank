package practice;

import java.util.Scanner;

public class FunnyString {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int T = scan.nextInt();
		String res = "";
		tloop: for (int t = 0; t < T; t++) {

			String ip = scan.next();
			String rip = (new StringBuilder(ip).reverse().toString());
			int len = ip.length();
			for (int i = 1; i < len; i++) {

				if (Math.abs(ip.charAt(i) - ip.charAt(i - 1)) != Math.abs(rip.charAt(i) - rip.charAt(i - 1))) {

					res += "Not Funny\n";
					continue tloop;

				}

			}
			res += "Funny\n";

		}
		System.out.println(res);
	}

}
