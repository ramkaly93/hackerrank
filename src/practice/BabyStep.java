package practice;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class BabyStep {

	public static void main(String[] args) throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());

		PrintWriter pr = new PrintWriter(new OutputStreamWriter(System.out));

		int Q = Integer.parseInt(st.nextToken());

		for (int q = 0; q < Q; q++) {
			st = new StringTokenizer(br.readLine());
			int a = Integer.parseInt(st.nextToken());
			int b = Integer.parseInt(st.nextToken());
			int d = Integer.parseInt(st.nextToken());

			pr.println(calc(a, b, d));

		}
		pr.flush();

	}

	private static int calc(int a, int b, int d) {

		int count = 0;
		int min = Math.min(a, b);
		int max = Math.max(a, b);
		int D = d;
		if (d == 0)
			return 0;

		if (d % max == 0)
			return d / max;
		if (d == min | d == max)
			return 1;
		if (d < max)
			return 2;

		else {

			int otherSidesum = a + b;

			d = D / max;
			if (D - (d * max) <= a + b)
				d = d - 1;
			count += d;

		}

		count += 2;

		return count;

	}

}
