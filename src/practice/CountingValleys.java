package practice;

import java.util.Scanner;

public class CountingValleys {
	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		int n = scan.nextInt();
		String ip = scan.next();

		int atSeaLevel = 1, climbing = 0, drowning = 0, nod = 0, nou = 0;
		int res = 0;
		for (int i = 0; i < ip.length(); i++) {

			char step = ip.charAt(i);
			if (step == 'U' && atSeaLevel == 1) {
				climbing = 1;
				atSeaLevel = 0;
				nou = 1;
				nod = 0;

			}

			else if (step == 'D' && atSeaLevel == 1) {
				drowning = 1;
				atSeaLevel = 0;
				nod = 1;
				nou = 0;
				res++;

			}

			else {

				if (step == 'D')
					nod++;

				if (step == 'U')
					nou++;
				if (nod == nou) {

					atSeaLevel = 1;

				}
			}

		}
		System.out.println(res);

	}

}
