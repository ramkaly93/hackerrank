package practice;

import java.util.Arrays;
import java.util.Scanner;

public class PermutingTwoArrays {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		String res = "";
		int Q = scan.nextInt();
		for (int q = 0; q < Q; q++) {

			int N = scan.nextInt();
			int K = scan.nextInt();
			int[] A = new int[N];
			int[] B = new int[N];

			for (int i = 0; i < N; i++)
				A[i] = scan.nextInt();

			for (int i = 0; i < N; i++)
				B[i] = scan.nextInt();

			Arrays.sort(A);
			Arrays.sort(B);
			boolean flag = true;
			for (int i = 0, j = N - 1; i < N & j >= 0; i++, j--) {

				if (A[i] + B[j] < K)
					flag = false;

			}
			if (flag)
				res += "YES\n";
			else
				res += "NO\n";

		}
		System.out.println(res);

	}
}
