package practice;

import java.util.Arrays;
import java.util.Scanner;

public class MarkandToys {
	public static void main(String[] args) {
		Scanner stdin = new Scanner(System.in);
		int n = stdin.nextInt(), k = stdin.nextInt();
		int prices[] = new int[n];
		for (int i = 0; i < n; i++)
			prices[i] = stdin.nextInt();

		Arrays.sort(prices);

		int answer = 0;
		int i = 0;
		while (k - prices[i] > 0) {

			answer++;
			k = k - prices[i];
			i++;
			// System.out.println("k is "+ k+ " ans is "+ answer);

		}
		// Compute the final answer from n,k,prices
		System.out.println(answer);
	}

}
