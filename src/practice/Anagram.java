package practice;

import java.util.Scanner;

public class Anagram {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int T = scanner.nextInt();
		String fres = "";
		for (int t = 0; t < T; t++) {

			String ip = scanner.next();
			// System.out.println("len is " +ip.length());
			if (ip.length() % 2 != 0) {

				fres += "-1" + "\n";
				continue;

			}

			String first = ip.substring(0, (ip.length() / 2));
			String second = ip.substring(ip.length() / 2, ip.length());
			/*
			 * System.out.println("first:"+first);
			 * System.out.println("second:"+second);
			 */

			int fcount[] = new int[26];
			int scount[] = new int[26];
			for (int i = 0; i < 26; i++)
				fcount[i] = scount[i] = 0;

			for (int i = 0; i < first.length(); i++) {

				fcount[first.charAt(i) - 97] += 1;
				scount[second.charAt(i) - 97] += 1;

			}
			int res = 0;
			for (int i = 0; i < 26; i++) {

				// System.out.println("fcount:"+fcount[i]+"scount:"+scount[i]);
				res += Math.abs(fcount[i] - scount[i]);

			}
			res = res / 2;
			fres += res + "\n";
		}

		System.out.println(fres);

	}

}
