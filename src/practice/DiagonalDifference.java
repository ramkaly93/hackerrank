package practice;

import java.util.Scanner;

public class DiagonalDifference {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int a[][] = new int[n][n];

		int mainDiaganalSum = 0;
		int otherDiaganalSum = 0;
		for (int a_i = 0; a_i < n; a_i++) {
			for (int a_j = 0; a_j < n; a_j++) {
				a[a_i][a_j] = in.nextInt();
				if (a_i == a_j)
					mainDiaganalSum += a[a_i][a_j];

			}
		}
		// System.out.println("md" + mainDiaganalSum);
		int i = n - 1;
		int j = 0;

		while (j <= n - 1) {

			// System.out.println("ele " +a[i][j]);
			otherDiaganalSum += a[i][j];
			i = i - 1;
			j = j + 1;

		}
		// System.out.println("od" + otherDiaganalSum);
		System.out.println(Math.abs(mainDiaganalSum - otherDiaganalSum));

	}

}
