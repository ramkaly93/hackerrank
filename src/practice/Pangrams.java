package practice;

import java.util.Scanner;

public class Pangrams {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		String ip = scan.nextLine();
		ip = ip.toLowerCase();
		for (char c = 'a'; c <= 'z'; c++) {

			if (ip.indexOf(c) == -1) {
				System.out.println("not pangram");
				return;
			}

		}

		System.out.println("pangram");

	}

}
