package practice;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class Cipher {

	public static void main(String[] args) throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		PrintWriter pr = new PrintWriter(new OutputStreamWriter(System.out));

		int N = Integer.parseInt(st.nextToken());
		int K = Integer.parseInt(st.nextToken());

		String ip = br.readLine();

		char[] ipArr = ip.toCharArray();
		char[] fres = new char[N];
		int fIndex = 1;
		fres[0] = ipArr[0];
		
		pr.print(fres[0]-48);
		int xorSoFar = ipArr[0] - 48;
		for (int i = 1; i < N - 1; i++) {

			int val = ipArr[i] - 48;
			//System.out.println("val is" + val);
			char res = '0';
			if (i - K >= 0)
				xorSoFar = xorSoFar ^ (fres[i - K] - 48);

			if (xorSoFar != val) {
				res = '1';

			}

			pr.print(res);
			fres[fIndex] = res;
			fIndex++;
			xorSoFar = xorSoFar ^ (res-48);

		}

		// fres += ip.charAt(ip.length()-1 );
		// System.out.println(fres);

		pr.println(ip.charAt(ip.length() - 1));
		pr.flush();

	}
}
