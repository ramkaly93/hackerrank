package practice;

import java.util.Scanner;

public class AlternatingCharacters {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int T = scan.nextInt();
		String res = "";
		for (int t = 0; t < T; t++) {

			String ip = scan.next();
			int cnt = 0;
			for (int i = 0; i < ip.length() - 1; i++) {
				if (ip.charAt(i) == ip.charAt(i + 1))
					cnt++;

			}
			res += cnt + "\n";

		}
		System.out.println(res);

	}

}
