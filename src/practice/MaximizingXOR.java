package practice;

import java.util.Scanner;

public class MaximizingXOR {

	static int maxXor(int l, int r) {
		int max = l ^ r;

		for (int i = l; i <= r; i++) {

			for (int j = l; j <= r; j++) {

				int tmp = i ^ j;
				if (tmp > max)
					max = tmp;

			}

		}
		return max;

	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int res;
		int _l;
		_l = Integer.parseInt(in.nextLine());

		int _r;
		_r = Integer.parseInt(in.nextLine());

		res = maxXor(_l, _r);
		System.out.println(res);

	}

}
