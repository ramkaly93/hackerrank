package practice;

import java.util.Scanner;

public class LonelyInteger {

	private static int lonelyInteger(int[] arr) {
		int res = arr[0];
		for (int i = 1; i < arr.length; i++)

			res = res ^ arr[i];

		return res;

	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int[] a = new int[n];

		for (int i = 0; i < n; i++) {
			a[i] = in.nextInt();
		}
		System.out.println(lonelyInteger(a));
	}
}
