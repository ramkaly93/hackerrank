package practice;

import java.util.Scanner;

public class SherlockandArray {
	public static void display(long[] arr) {
		System.out.println("Contents of the array \n");

		for (long i : arr)
			System.out.print(i + "   ");

		System.out.println("\n");

	}

	public static String sum(long[] arr) {
		long[] res = new long[arr.length];
		long[] res1 = new long[arr.length];

		res[0] = 0;
		for (int iter = 1; iter < res.length; iter++)
			res[iter] = res[iter - 1] + arr[iter - 1];

		res1[res1.length - 1] = 0;
		for (int iter = res1.length - 2; iter >= 0; iter--)
			res1[iter] = res1[iter + 1] + arr[iter + 1];

		for (int iter = 0; iter < arr.length; iter++)
			if (res[iter] == res1[iter])
				return "YES";

		return "NO";

	}

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in).useDelimiter("\\s+");
		long nTestCases = scan.nextLong();
		String res = "";
		long i = 0;

		while (i < nTestCases) {

			int no_elements = scan.nextInt();
			long[] arr = new long[no_elements];
			int iter = 0;

			while (iter < no_elements)
				arr[iter++] = scan.nextLong();

			res += sum(arr) + "\n";
			i++;
		}
		System.out.println(res);
	}

}
