package practice;

import java.util.Hashtable;
import java.util.Scanner;

public class MinimumDistances {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		Hashtable ht = new Hashtable();
		int min = 100002;
		for (int A_i = 0; A_i < n; A_i++) {
			Integer ele = in.nextInt();

			if (ht.containsKey(ele)) {

				Integer temp = ((Integer) ht.get(ele)).intValue();
				temp = Math.abs(temp - A_i);
				if (temp < min)
					min = temp;

			}

			ht.put(ele, new Integer(A_i));

		}

		System.out.println((min == 100002) ? "-1" : ("" + min));

	}

}
