package practice;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class StringConstruction {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();

		String res = "";
		for (int a0 = 0; a0 < n; a0++) {
			String source = in.next();

			Set<Character> set = new HashSet<Character>();
			for (int i = 0; i < source.length(); i++)
				set.add(source.charAt(i));

			res += (set.size()) + "\n";

		}

		System.out.println(res);
	}

}
