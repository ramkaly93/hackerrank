package practice;

import java.util.Scanner;

public class UtopianTree {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int t = in.nextInt();
		String res = "";
		for (int a0 = 0; a0 < t; a0++) {
			int n = in.nextInt();
			int temp = 1;
			int flag = 0;
			while (n-- > 0) {
				temp = (flag == 0) ? temp * 2 : temp + 1;

				if (flag == 0)
					flag = 1;
				else
					flag = 0;

			}
			res += temp + "\n";
		}
		System.out.println(res);
	}

}
